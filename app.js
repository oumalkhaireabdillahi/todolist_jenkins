//jshint esversion:6

const express = require("express");
const bodyParser = require("body-parser");
//const date = require(__dirname + "/date.js");
var minimatch = require("minimatch")
const app = express();


app.set('view engine', 'ejs');



app.use(bodyParser.urlencoded({extended: true}));

//app.use(express.static("public"));


app.use(express.static("public"));


const items = ["Buy Food", "Cook Food", "Eat Food"];
const workItems = [];

app.get("/", function(req, res) {

//const day = date.getDate();

let today = new Date();

let options = {
  weekday: 'long',
  year: 'numeric',
  month: 'long',
  day: 'numeric'
};



let day = today.toLocaleDateString("en-US", options);
  res.render("list", {listTitle: day, newListItems: items});

});

app.post("/", function(req, res){

  const item = req.body.newItem;

  if (req.body.list === "Work") {
    workItems.push(item);
    res.redirect("/work");
  } else {
    items.push(item);
    res.redirect("/");
  }
});

app.get("/work", function(req,res){
  res.render("list", {listTitle: "Work List", newListItems: workItems});
});

app.get("/about", function(req, res){
  res.render("about");
});

module.exports = app;

//app.listen(3000, function() {
  //console.log("Server started on port 3000");
//});

